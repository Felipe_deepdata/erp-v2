// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: '//localhost/aguagente/api/public',
  // apiUrl: 'https://panelaguagente.xyz:8073/demo/v3/api/public'
  months_ahead: [
    {
      id: 1,
      name: 1
    }, 
    {
      id: 3,
      name: 3
    }, 
    {
      id: 6,
      name: 6
    }, 
    {
      id: 12,
      name: 12
    }
  ],
  payment_types: [
    {
      name: 'OXXO',
      id: 'OXXO'
    }, 
    {
      name: 'SPEI',
      id: 'SPEI'
    }
  ],
  error_codes_categories: [
    {
      id: 'Basic Codes',
      name: 'Basic Codes'
    },
    {
      id: 'Detailed Codes',
      name: 'Detailed Codes'
    },
    {
      id: 'Other',
      name: 'Other'
    }
  ]
};
