import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ChargesService {
  baseUrl = `${environment.apiUrl}`;
  constructor(private readonly http: HttpClient) { }

  show(id): any {
    return this.http.get(`${this.baseUrl}/charges/${id}`);
  }

  refund(id, params): any {
    return this.http.post(`${this.baseUrl}/charges/${id}/refund`, params);
  }

  getClientCharges(params, filters?): any {
    let id = 0;
    if (filters) {
      id = filters.id_clients;
    }
    
    return this.http.post(`${this.baseUrl}/client/${id}/getClientChargesDataTable`, params);
  }

  getChargeRefunds(params, filters?): any {
    let id = 0;
    if (filters) {
      id = filters.id_clients;
    }

    return this.http.post(`${this.baseUrl}/charge/${id}/refunds`, params);
  }
}
