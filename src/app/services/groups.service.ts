import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GroupsService {
    baseUrl = `${environment.apiUrl}`;

    constructor(private readonly http: HttpClient) { }
    create(params): any {
        return this.http.post(`${this.baseUrl}/groups`, params);
    }

    delete(id): any {
        return this.http.delete(`${this.baseUrl}/groups/${id}`);
    }

    show(id): any {
        return this.http.get(`${this.baseUrl}/groups/${id}`);
    }

    update(id, params): any {
        return this.http.put(`${this.baseUrl}/groups/${id}`, params);
    }

    getGroups(params?): any {
        return this.http.post(`${this.baseUrl}/groups/get_groups`, params);
    }
    
    getNotifications(params?): any {
        return this.http.post(`${this.baseUrl}/notification_groups/get`, params);
    }

    saveNotificationGroup(params): any {
        return this.http.post(`${this.baseUrl}/notification_groups/save`, params);
    }

    getClientNotification(id, params?): any {
        return this.http.post(`${this.baseUrl}/notification_groups/get_users/${id}`, params);
    }

    editClientNotification(id, params?): any {
        return this.http.post(`${this.baseUrl}/notification_groups/edit/${id}`, params);
    }
    
    deleteGroup(id): any {
        return this.http.delete(`${this.baseUrl}/notification_groups/${id}`);
    }
}
