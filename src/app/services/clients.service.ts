import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, forkJoin } from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class ClientsService {
  baseUrl = `${environment.apiUrl}`;

  constructor(private readonly http: HttpClient) { }

  getClients(params?, filters?): Observable<any> {
    if (filters) {
      Object.assign(params, filters);
      // params['status'] = filters['status'];
      // params['serial_number'] = filters['serial_number'];
      // params['sales_agent'] = filters['sales_agent'];
    }

    return this.http.post(`${this.baseUrl}/clients/get_clients`, params);
  }

  show(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${id}`);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/clients/${id}`);
  }

  setStatus(id?, params?): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/setStatus`, params);
  }

  subscribeToPlan(id, params?): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/subscribeToPlan`, params);
  }

  subscribeToPlanOffline(id, params): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/subscribeToPlanOffline`, params);
  }

  makeSalesAgent(id?, params?): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${id}/attachRole`, params);
  }

  update(id, params?): Observable<any> {
    return this.http.put(`${this.baseUrl}/clients/${id}`, params);
  }

  getHierarchicalTree(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${id}/hierarchicalTree`);
  }

  getCards(params, filters?): Observable<any> {
    let id = 0;
    if (filters) {
      id = filters.id_clients;
    }

    return this.http.post(`${this.baseUrl}/client/${id}/getCards`, params);
  }

  getImages(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${id}/getImages`);
  }

  getHistory(id_clients): any {
    const params = {
      params: { id_clients }
    };
    const corrections   = this.http.get(`${this.baseUrl}/corrections`, params);
    const invalidations = this.http.get(`${this.baseUrl}/invalidations`, params);
    const rejections    = this.http.get(`${this.baseUrl}/rejections`, params);
    const cancellations = this.http.get(`${this.baseUrl}/cancellations`, params);
    // const notifications = this.http.get(`${this.baseUrl}/notifications`, params);

    return forkJoin([corrections, invalidations, rejections, cancellations]);
  }
}
