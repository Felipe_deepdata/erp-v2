import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUrl = `${environment.apiUrl}`;

  constructor(
    private readonly http: HttpClient
  ) {
  }

  user(val): any {
    return this.http.get(`${this.baseUrl}/users/${val}`);
  }

  getAdmins(payload): any {
    return this.http.post(`${this.baseUrl}/users/Administrator`, payload);
  }

  getTechnitians(payload): any {
    return this.http.post(`${this.baseUrl}/users/Technician`, payload);
  }

  getUserByRole(params, filter): any {
    const role = filter.role.role;

    return this.http.post(`${this.baseUrl}/users/${role}`, params);
  }

  getImageProfile(id): any {
    return this.http.get(`${this.baseUrl}/users/${id}/getImageProfile`);
  }

  create(params): any {
    return this.http.post(`${this.baseUrl}/auth/register`, params);
  }

  show(id): any {
    return this.http.get(`${this.baseUrl}/users/${id}`);
  }

  update(id, params): any {
    return this.http.put(`${this.baseUrl}/users/${id}`, params);
  }
  getUser(params?): any {
    return this.http.post(`${this.baseUrl}/users/getUsers`, params);
  }
}
