import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DebtsService {
  baseUrl = `${environment.apiUrl}`;

  constructor(private readonly http: HttpClient) { }

  getClientDebts(params?, filters?): any {
    Object.assign(params, filters);
    
    return this.http.post(`${this.baseUrl}/debts`, params);
  }

  chargeDebt(id, params?): any {
    return this.http.post(`${this.baseUrl}/debts/${id}/charge`, params);
  }
}
