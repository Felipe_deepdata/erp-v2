import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {
  @Input('item') item: any;

  // tslint:disable-next-line: typedef
  ngOnInit() {
    // tslint:disable-next-line: no-empty
  }

}
