import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  @Input('type') type: any;
  @Input('item') item: any;

  // tslint:disable-next-line: typedef
  ngOnInit() {
    // tslint:disable-next-line: no-empty
  }

}
