import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormFieldComponent } from './form-field/form-field.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  declarations: [
    FormFieldComponent
  ],
  entryComponents: [
    FormFieldComponent
  ],
  exports: [
    FormFieldComponent
  ],
  providers: [
    FormFieldComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ]
})
export class FormModule { }
