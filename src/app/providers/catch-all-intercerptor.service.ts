import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SwalService } from '../services/swal.service';
import { AuthorizationService } from '../services/authorization.service';

@Injectable({
  providedIn: 'root'
})

export class CatchAllInterceptorService implements HttpInterceptor {

  constructor(
    private readonly router: Router,
    public authService: AuthorizationService,
    public swal: SwalService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError(error => {
        let errorMessage: any;
        let displayMessage: any;

        if (error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Client-side error: ${error.error.message}`;
          this.swal.error({ text: errorMessage }).catch();
        } else {
          if (error.error.error === 'user_not_found') {
            errorMessage = 'Sesion expirada';
            this.authService.logout();
            this.swal.warning({title: 'Sesion expirada', text: '', showCancelButton: false, confirmButtonText: 'Ok'});
            this.router.navigateByUrl('/login');
          } else {
            // backend error
            errorMessage = `Server-side error: ${error.status} ${error.message}`;
            const errors = [
              `${error.status} - ${error.statusText}`,
              error.url
            ];
            
            displayMessage = '<div class="server-error">';
            displayMessage += '<p>Server-side error:</p>';
            displayMessage += `<ul>${this.returnFormated(errors)}</ul>`;
            displayMessage += '</div>';

            switch (error.status) {
              case 401:
                errorMessage = error;
                break;
              default:
                this.swal.error({ html: displayMessage }).catch();
                break;
            }
          }
        }

        // aquí podrías agregar código que muestre el error en alguna parte fija de la pantalla.
        // this.errorService.show(errorMessage);
        // console.warn(errorMessage);
        return throwError(errorMessage);
      })
    );
  }

  returnFormated(errors: Array<any>): string {
    let text = '';
    errors.forEach((error: string) => {
      text += `<li>${error}</li>`;
    });

    return text;
  }
}
