import { Component, OnInit, Input } from '@angular/core';
import { ClientsService } from '../../../services/clients.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SwalService } from '../../../services/swal.service';
import { FromService } from '../../../providers/form.service';
import { ErrrorCodesService } from '../../../services/error-codes.service';
import { TicketsService } from '../../../services/tickets.service';

@Component({
  selector: 'app-create-support-ticket',
  templateUrl: './create-support-ticket.component.html',
  styleUrls: ['./create-support-ticket.component.scss']
})
export class CreateSupportTicketComponent implements OnInit {
  @Input() data: any;
  client: any;
  error_codes: [];
  ticket_types: [];
  form: FormGroup = this.formBuilder.group({
    id_clients: [],
    id_error_codes: ['', Validators.required],
    description: ['', Validators.required],
    estimated_service_fee: [''],
    estimated_service_fee_reasons: [''],
    type: ['', Validators.required],
    status: ['opened']
  });

  constructor(
    public activeModal: NgbActiveModal,
    private readonly formBuilder: FormBuilder,
    private readonly fromService: FromService,
    private readonly clientService: ClientsService,
    private readonly errorCodesService: ErrrorCodesService,
    private readonly ticketsService: TicketsService,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    this.clientService.show(this.data.id_clients).subscribe((data: any) => {
      this.client = data.response;
    });
    this.errorCodesService.index().subscribe((data: any) => {
      this.error_codes = data.response;
    });
    this.ticketsService.getTicketTypes().subscribe((data: any) => {
      this.ticket_types = data.response;
    });
    this.fromService.setForm(this.form);
  }

  createSupportTicket(): void {
    // this.form.get('id_error_codes').value;
    // this.form.get('type').value;
    this.form.controls.id_clients.setValue(this.client.id_clients);

    if (this.form.valid) {
      this.ticketsService.create(this.form.value).subscribe((data: any) => {
        if (data.success) {
          this.swal.success({ title: 'Ticket creado exitosamente' }).then(() => {
            this.activeModal.dismiss();
          });
        } else {
          this.swal.error({ title: 'Ocurrio un error al crear el ticket' });
        }
      });
    }
  }

}
