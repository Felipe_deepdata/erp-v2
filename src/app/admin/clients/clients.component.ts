import { Component, OnInit, OnDestroy } from '@angular/core';
import { BroadcastService } from '../../services/broadcast.service';
import { ModalComponent } from '../../components/modal/modal.component';
import { ClientsService } from '../../services/clients.service';
import { Subscription } from 'rxjs';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { ContractComponent } from '../../admin/clients/contract/contract.component';
import { SharedComponent } from '../../model/shared-component';
import { BadDebtsService } from '../../services/bad-debts.service';
import { SwalService } from '../../services/swal.service';
import { ReferalsComponent } from './referals/referals.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { CreateSupportTicketComponent } from './create-support-ticket/create-support-ticket.component';
import { ClientChargesHistoryComponent } from './client-charges-history/client-charges-history.component';
import { ClientHistoryComponent } from './client-history/client-history.component';
import { OfflinePaymentComponent } from './contract/offline-payment/offline-payment.component';
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnDestroy, OnInit {
  sr = 0;
  dataTableConfig = {
    config: {
      base: this.clients,
      api: 'getClients',
      order: [[2, 'desc']]
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'client_data'
      },
      {
        display: 'Grupo de ventas',
        field: 'signed_in_group',
        type: 'client_sales_group',
        orderable: false
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Suscripción',
        field: 'subscription_status',
        type: 'label',
        orderable: false,
        options: {
          emptyText: 'Faltante'
        }
      },
      {
        display: 'Email',
        field: 'email',
        type: 'text',
        orderable: false
      },
      {
        display: 'Estatus',
        field: 'status',
        type: 'label',
        orderable: false,
        options: {
          emptyText: 'N/A'
        }
      },
      {
        display: 'Sig. Pago',
        field: 'next_payday',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          [
            {
              display: 'Ver contrato de compra',
              icon: 'fa fa-file-text',
              event: 'client.contract',
              conditionality: 'true'
            },
            {
              display: 'Ver contratos referidos',
              icon: 'fa fa-file-o',
              event: 'client.referals',
              conditionality: 'this.data.status ===  "accepted"'
            },
            {
              display: 'Historial del cliente',
              icon: 'fa fa-search',
              event: 'client.clientHistory',
              conditionality: 'true'
            }
          ],
          [
            {
              display: 'Crear cargo OXXO/SPEI',
              icon: 'fa fa-dollar',
              event: 'client.offlinePayment',
              conditionality: 'this.data.status === "accepted"'
            },
            {
              display: 'Ver cargos',
              icon: 'fa fa-dollar',
              event: 'client.clientChargeHistory',
              conditionality: 'this.data.status === "accepted" || this.data.status === "cancelled"'
            }
          ],
          [
            {
              display: 'Editar',
              icon: 'fa fa-pencil',
              event: 'client.clientEdit',
              conditionality: 'true'
            },
            {
              display: 'Restablecer contraseña',
              icon: 'fa fa-key',
              event: 'client.restorePassword',
              conditionality: 'true'
            },
            {
              display: 'Activar como agente de Ventas',
              icon: 'fa fa-handshake-o',
              event: 'client.makeSalesAgent',
              conditionality: 'this.data.sales_agent !== 1 && this.data.status === "accepted"'
            },
            {
              display: 'Desactivar como agente de Ventas',
              icon: 'fa fa-handshake-o',
              event: 'client.removeSalesAgent',
              conditionality: 'this.data.sales_agent === 1'
            }
          ],
          [
            {
              display: 'Aceptar cliente',
              event: 'client.setAccepted',
              conditionality: 'this.data.status === "standby"'
            },
            {
              display: 'Invalidar cliente',
              event: 'client.setInvalid',
              conditionality: 'this.data.status === "standby"'
            },
            {
              display: 'Rechazar cliente',
              event: 'client.setRejected',
              conditionality: 'this.data.status === "standby" || this.data.status === "invalid"'
            },
            {
              display: 'Cancelar cliente',
              event: 'client.setCancelled',
              conditionality: 'this.data.status === "accepted"'
            }
          ],
          [
            {
              display: 'Eliminar cliente',
              event: 'client.delete',
              conditionality: 'this.data.status === "rejected" || this.data.status === "prospect"'
            },
            {
              display: 'Mover a mala deuda',
              event: 'client.moveToBadDebt',
              conditionality: 'this.data.status === "accepted" && this.data.bad_debt === 0'
            }
          ],
          [
            {
              display: 'Crear ticket de soporte',
              icon: 'fa fa-ticket',
              event: 'client.createSupportTicket',
              conditionality: 'this.data.status === "accepted"'
            }
          ]
        ]
      }
    ],
    filters: [
      {
        title: 'Estatus',
        cssClass: 'col-md-4',
        type: 'status',
        options: [
          {
            val: '',
            name: 'TODOS'
          },
          {
            val: 'standby',
            name: 'Pendiente'
          },
          {
            val: 'invalid',
            name: 'Invalido'
          },
          {
            val: 'canceled',
            name: 'Cancelado'
          },
          {
            val: 'rejected',
            name: 'Rechazado'
          },
          {
            val: 'accepted',
            name: 'Aceptado'
          }
        ]
      },
      {
        title: 'Instalación',
        cssClass: 'col-md-4',
        type: 'serial_number',
        options: [
          {
            val: '',
            name: 'TODOS'
          },
          {
            val: 'instaled',
            name: 'Instalado'
          },
          {
            val: '',
            name: 'No instalado'
          }
        ]
      },
      {
        title: 'Agente de ventas',
        cssClass: 'col-md-4',
        type: 'sales_agent',
        options: [
          {
            val: '',
            name: 'TODOS'
          },
          {
            val: '1',
            name: 'Activo'
          },
          {
            val: '0',
            name: 'Inactivo'
          }
        ]
      }
    ]
  };

  contractData = {
    id_clients: 0,
    monthly_fee: 0,
    sr: 0,
    bank_fee: 0,
    coupon: 0,
    total: 0
  };

  broadcast$: Subscription;

  constructor(
    private readonly clients: ClientsService,
    private readonly badDebts: BadDebtsService,
    private readonly broadcast: BroadcastService,
    public appModal: ModalComponent,
    public swal: SwalService
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'client.contract': this.contractItem(event.data); break;
        case 'client.referals': this.referalsItem(event.data); break;
        case 'client.clientHistory': this.clientHistoryItem(event.data); break;
        case 'client.clientChargeHistory': this.clientChargeHistoryItem(event.data); break;
        case 'client.clientEdit': this.clientEditItem(event.data); break;
        case 'client.delete': this.clientDeleteItem(event.data); break;
        case 'client.makeSalesAgent': this.makeSalesAgentItem(event.data); break;
        case 'client.removeSalesAgent': this.removeSalesAgentItem(event.data); break;
        case 'client.setAccepted': this.setAcceptedItem(event.data); break;
        case 'client.setInvalid': this.setBadStatusItem(event.data, event.name); break;
        case 'client.setRejected': this.setBadStatusItem(event.data, event.name); break;
        case 'client.setCancelled': this.setBadStatusItem(event.data, event.name); break;
        case 'client.moveToBadDebt': this.moveToBadDebtItem(event.data); break;
        case 'client.offlinePayment': this.offlinePaymentItem(event.data); break;
        case 'client.createSupportTicket': this.createSupportTicketItem(event.data); break;
        case 'client.restorePassword': this.restorePasswordItem(event.data); break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  createSupportTicketItem(data): void {
    const props: SharedComponent = new SharedComponent(CreateSupportTicketComponent, data, { title: `Crear ticket de soporte para el cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  contractItem(data): void {
    const props: SharedComponent = new SharedComponent(ContractComponent, data, { title: `Contrato de compra del cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  clientEditItem(data): void {
    const props: SharedComponent = new SharedComponent(ClientEditComponent, data, { title: 'Edición del cliente' });
    this.appModal.openXl(props);
  }

  clientDeleteItem(data): void {

    this.swal.warning({ title: '¿Estás seguro de eliminar al cliente?' }).then(result => {
      if (result.value) {
        this.clients.delete(data.id_clients).subscribe((response: any) => {
          if (response.success) {
            this.swal.success().then(() => {
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({ text: response.message });
          }
        });
        // this.swal.success();
        // this.broadcast.reloadDataTable();
      }
    });
  }

  clientChargeHistoryItem(data): void {
    const props: SharedComponent = new SharedComponent(ClientChargesHistoryComponent, data, { title: `Historial de cargos del cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  clientHistoryItem(data): void {
    const props: SharedComponent = new SharedComponent(ClientHistoryComponent, data, { title: `Historial del cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  offlinePaymentItem(data): void {
    this.contractData.monthly_fee = data.monthly_fee / 100;
    this.contractData.total = this.total(data);
    this.contractData.sr = this.contractData.monthly_fee / 1.16 * 0.007;
    const props: SharedComponent = new SharedComponent(
      OfflinePaymentComponent,
      {
        client: data,
        contract: this.contractData,
        firstCharge: false
      },
      { title: 'Pago por OXXO / SPEI' });
    this.appModal.openXl(props);
  }

  referalsItem(data): void {
    const props: SharedComponent = new SharedComponent(ReferalsComponent, data, { title: `Contratos referidos del cliente ${data.name}` });
    this.appModal.openXl(props);
  }

  setAcceptedItem(data): void {
    const params = { status: 'accepted' };

    this.swal.warning({ title: '¿Estás seguro de aceptar al cliente?' }).then(result => {
      if (result.value) {
        this.clients.setStatus(data.id_clients, params).toPromise();
        this.swal.success().then(() => {
          this.broadcast.reloadDataTable();
        });
      }
    });
  }

  setBadStatusItem(data, event): void {

    let title_status: string;
    let status: string;

    switch (event) {
      case 'client.setCancelled':
        title_status = 'cancelado';
        status = 'canceled';
        break;

      case 'client.setInvalid':
        title_status = 'invalidado';
        status = 'invalid';
        break;

      case 'client.setRejected':
        title_status = 'rechazado';
        status = 'rejected';
        break;
    }

    title_status = `Favor de proporcionar las razones por las que el cliente será ${title_status}`;
    const swalParams = {
      title: 'Motivos',
      text: title_status,
      inputPlaceholder: 'Razones'
    };

    this.swal.input(swalParams).then((resp: any) => {
      if (resp.value) {
        this.clients.setStatus(data.id_clients, { status, reasons: resp.value }).subscribe((response: any) => {
          if (response.success) {
            this.swal.success().then(() => {
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({ text: response.message });
          }
        });
      }
    });
  }

  moveToBadDebtItem(data): void {

    const swalParams = {
      title: 'Mover a mala deuda',
      text: 'Favor de proporcionar las razones por las que el cliente sera movido a mala deuda',
      inputPlaceholder: 'Razones'
    };

    this.swal.input(swalParams).then((resp: any) => {
      if (resp.value) {
        this.badDebts.moveToBadDebt(data.id_clients, { reason: resp.value }).subscribe((response: any) => {
          if (response.success) {
            this.swal.success().then(() => {
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error();
          }
        });
      }
    });
  }

  makeSalesAgentItem(data): void {
    const params = { role: 'Sales Agent' };
    const swal_title = `Estás seguro de cambiar el nivel del cliente ${data.name}`;

    this.swal.warning({ title: swal_title }).then(result => {
      if (result.value) {
        this.clients.makeSalesAgent(data.id_clients, params).subscribe((response: any) => {
          if (response.success) {
            this.swal.success().then(() => {
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({ title: response.message });
          }
        });
      }
    });
  }

  removeSalesAgentItem(data): void {
    //
  }

  restorePasswordItem(data): void {
    const props: SharedComponent = new SharedComponent(RestorePasswordComponent, data, { title: `Restaurar contraseña del cliente ${data.name}` });
    this.appModal.opensm(props);
  }

  total(client): any {

    if (client.social_responsability === 1) {
      this.contractData.sr = client.monthly_fee / 1.16 * 0.007;
    }
    const total = (client.monthly_fee / 100) + this.sr + 100;

    return total;
  }

}
