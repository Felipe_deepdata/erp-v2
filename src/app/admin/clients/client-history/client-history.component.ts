import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ClientsService } from '../../../services/clients.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-client-history',
  templateUrl: './client-history.component.html',
  styleUrls: ['./client-history.component.scss']
})
export class ClientHistoryComponent implements OnInit {
  @Input() data: any;
  historyConfig = {
    config: {
      type: 'tickets',
      base: this.clientService,
      api: 'getHistory',
      params: { history: 0 },
      order: [[0, 'desc']]
    },
    rows: [
      {
        display: 'Estatus',
        field: 'id'
      }
    ]
  };
  history = [];
  hist$: Subscription;

  constructor(
    private readonly clientService: ClientsService,
    public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.hist$ = this.clientService.getHistory(this.data.id_clients).subscribe((data: any) => {
      data.forEach((element, index) => {
        let type = '';
        switch (index) {
          case 0: type = 'corrections'; break;
          case 1: type = 'invalidations'; break;
          case 2: type = 'rejections'; break;
          case 3: type = 'cancellations'; break;
          // case 4: type = 'notifications'; break;
        }
        element.response.forEach(itm => {
          this.history.push({ type, ...itm});
        });
        this.sortData();
      });
    });
  }

  sortData(): Array<any> {
    return this.history.sort((a, b) =>
      (new Date(b.created_at) as any) - (new Date(a.created_at) as any));
  }

  ngOnDestroy(): void {
    this.hist$.unsubscribe();
  }

}
