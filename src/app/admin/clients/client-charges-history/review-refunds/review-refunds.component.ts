import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ChargesService } from '../../../../services/charges.service';

@Component({
  selector: 'app-review-refunds',
  templateUrl: './review-refunds.component.html',
  styleUrls: ['./review-refunds.component.scss']
})
export class ReviewRefundsComponent implements OnInit {
  @Input() data: any;
  charge: any;

  dataTableConfig = {
    config: {
      base: this.chargeService,
      api: 'getChargeRefunds',
      params: {
        id_clients: 0
      }
    },
    columns: [
      {
        display: 'Cantidad',
        field: 'amount',
        type: 'text'
      },
      {
        display: 'Fecha del reembolso',
        field: 'created_at',
        type: 'text'
      }
    ],
    filters: [{}]
  };

  constructor(
    public activeModal: NgbActiveModal,
    private readonly chargeService: ChargesService
  ) { }

  ngOnInit(): void {
    this.chargeService.show(this.data.id_charges).subscribe((data: any) => {
      this.charge = data.response;
    });
    this.dataTableConfig.config.params.id_clients = this.data.id_charges;
  }

}
