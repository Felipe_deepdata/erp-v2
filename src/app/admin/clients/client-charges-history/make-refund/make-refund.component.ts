import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ChargesService } from '../../../../services/charges.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FromService } from '../../../../providers/form.service';
import { Subscription } from 'rxjs';
import { SwalService } from '../../../../services/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-make-refund',
  templateUrl: './make-refund.component.html',
  styleUrls: ['./make-refund.component.scss']
})
export class MakeRefundComponent implements OnInit {
  @Input() data: any;
  charge: any;
  refundData = {
    id_charges: 0,
    amount: 0,
    total_refunded: 0,
    max_amount_to_refund: 0,
    refunds: []
  };
  form: FormGroup = this.formBuilder.group({
    amount: ['', Validators.required]
  });
  broadcast$: Subscription;
  constructor(
    public activeModal: NgbActiveModal,
    private readonly formBuilder: FormBuilder,
    private readonly formService: FromService,
    private readonly chargeService: ChargesService,
    private readonly swal: SwalService) { }

  ngOnInit(): void {
    this.chargeService.show(this.data.id_charges).subscribe((data: any) => {
      this.charge = data.response;
      this.refundData.amount = this.charge.amount;
      this.refundData.refunds = this.charge.refunds;
      this.refundData.total_refunded = this.totalRefunded();
      this.refundData.max_amount_to_refund = this.maxAmountToRefund();
    });
    this.formService.setForm(this.form);
  }

  totalRefunded(): number {
    let total_refunded = 0;
    for (let i = 0; i < this.refundData.refunds.length; i++) {
      const amount = Number(this.charge.refunds[i].amount);
      total_refunded += amount;
    }

    return total_refunded;
  }

  maxAmountToRefund(): number {
    const maxRefund = this.refundData.amount - this.totalRefunded();

    return maxRefund;
  }

  makeRefund(): void {
    if (this.form.valid) {
      const params = this.form.value;
      this.swal.warning({ title: '¿Esta seguro de querer efectuar la siguiente devolución?' }).then(result => {
        if (result.value) {
          this.chargeService.refund(this.charge.id_charges, params).subscribe((data: any) => {
            if (data.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
              });
            } else {
              this.swal.error({ title: 'Ocurrió un error al realizar el reembolso' });
            }
          });
        }
      });
    }
  }
}
