import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientsService } from '../../../services/clients.service';
import { SharedComponent } from '../../../model/shared-component';
import { OfflinePaymentComponent } from './offline-payment/offline-payment.component';
import { ModalComponent } from '../../../components/modal/modal.component';
import { Subscription } from 'rxjs';
import { BroadcastService } from '../../../services/broadcast.service';
import { SwalService } from '../../../services/swal.service';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit, OnDestroy {
  @Input() data: any;
  baseUrl = `${environment.apiUrl}`;
  refeer: any;
  coupon = {};
  documents = [];
  photos = [];
  client: any;
  contractData = {
    id_clients: 0,
    deposit: 0,
    monthly_fee: 0,
    first_charge: 0,
    extras: 0,
    sr: 0,
    bank_fee: 0,
    installation_fee: 0,
    coupon: 0,
    total: 0
  };

  dataTableConfig = {
    config: {
      base: this.clientService,
      api: 'getCards',
      params: {
        id_clients: 0
      }
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Exp',
        field: 'exp_date',
        type: 'text'
      },
      {
        display: 'Tarjeta',
        field: 'last4',
        type: 'text'
      },
      {
        display: 'Marca',
        field: 'brand',
        type: 'text'
      }
    ],
    filters: [{}]
  };

  broadcast$: Subscription;
  
  constructor(
    private readonly clientService: ClientsService,
    private readonly swal: SwalService,
    private readonly broadcast: BroadcastService,
    public activeModal: NgbActiveModal,
    public appModal: ModalComponent
  ) {
  }

  ngOnInit(): void {
    this.clientService.show(this.data.id_clients).subscribe((resp: any) => {
      this.client = resp.response;
      this.contractData.id_clients = this.client.id_clients;
      this.broadcast$ = this.broadcast.events.subscribe(eventData => {/**/});
      this.contractData.deposit = this.client.deposit / 100;
      this.contractData.monthly_fee = this.client.monthly_fee / 100;
      this.contractData.installation_fee = this.client.installation_fee / 100;
      this.contractData.extras = this.extras();
      this.contractData.sr = Math.round(this.socialResponsability() * 100) / 100;
      this.contractData.first_charge = this.firstCharge();
      // this.contractData.first_charge = this.firstCharge() + this.contractData.sr;
      this.contractData.bank_fee = this.client.charge_fee / 100;
      this.contractData.coupon = this.discountCoupon();
      this.contractData.total = this.total();
      this.dataTableConfig.config.params.id_clients = this.client.id_clients;
      this.getRefeer();

      this.clientService.getImages(this.client.id_clients).subscribe((data: any) => {
        this.documents = data.response.documents;
        this.photos    = data.response.photos;
      });
    });

  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  extras(): number {
    let totalPrice = 0;
    for (let i = 0; i < this.client.contract.extras.length; i++) {
      const extra = this.client.contract.extras[i];
      let materialPrice = Number(extra.price);
    
      if (extra.element_name === 'Acero inoxidable') {
        materialPrice = this.contractData.installation_fee;
      }

      totalPrice += materialPrice;
    }
    
    return totalPrice;
  }

  firstCharge(): number {
    let first_charge = this.contractData.monthly_fee;
    if (this.client.signed_in_group !== null && this.client.signed_in_group.trial_days > 0) {
      const trial_days_price = parseInt(this.client.signed_in_group.trial_days_price, 10) / 100;

      // if (this.client.group.trial_days !== 0) {
      if (this.client.signed_in_group.trial_days !== 0) {
        first_charge = trial_days_price;
      }
    }

    return first_charge;
  }

  socialResponsability(): number {
    let client_responsability = 0;
    let material_responsability = 0;
    let social_responsability = 0;

    if (this.client.social_responsability === 1) {
      client_responsability = this.contractData.monthly_fee * 0.007;
    }

    for (let i = 0; i < this.client.contract.extras.length; i++) {
      const extra = this.client.contract.extras[i];
      let materialPrice = Number(extra.price);

      if (extra.element_name === 'Acero inoxidable') {
        materialPrice = this.contractData.installation_fee;
      }

      // social_responsability = ((materialPrice + this.contractData.monthly_fee) / 1.16 * .007);
      material_responsability += ((materialPrice / 1.16) * 0.007);
    }

    social_responsability = client_responsability + material_responsability;
    
    return social_responsability;
  }

  discountCoupon(): number {
    let discount = 0;
    if (this.client.id_coupon > 0) {
      discount = parseInt(this.client.coupon.reward, 10);
    }

    return discount;
  }

  getRefeer(): any {
    this.clientService.show(this.client.referred_by).subscribe((data: any) => {
      this.refeer = data.response;
    });
  }

  total(): number {
    // console.log('total: ', this.contractData.deposit, this.contractData.first_charge,  this.contractData.sr,  this.contractData.extras );
    // tslint:disable-next-line: max-line-length
    const total = (this.contractData.deposit + this.contractData.first_charge + this.contractData.sr + this.contractData.extras) - this.contractData.coupon;

    return total;
  }
  
  firstPayment(): void {
    this.swal.warning({title: '¿Estás seguro de continuar? se intentará recolectar el primer pago del cliente'}).then(result => {
        if (result.value) {
          this.clientService.subscribeToPlan(this.client.id_clients).subscribe((data: any) => {
            if (data.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
              });
            } else {
              this.swal.error({title: 'Ocurrior un error al procesar el pago'});
            }
          });
        }
    });
  }
  
  offlinePayment(): void {
    const props: SharedComponent = new SharedComponent(
      OfflinePaymentComponent,
      {
        client: this.data,
        contract: this.contractData,
        firstCharge: true
      },
      {
        title: 'Pago por OXXO / SPEI'
      }
    );
    this.appModal.openXl(props);
  }
}
