import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
// Services
import { TicketsService } from '../../../services/tickets.service';
import { BroadcastService } from '../../../services/broadcast.service';
import { SwalService } from '../../../services/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// Components
import { ModalComponent } from '../../../components/modal/modal.component';
import { SharedComponent } from '../../../model/shared-component';
import { RecordComponent } from './record/record.component';
import { AssignComponent } from './assign/assign.component';
import { CloseComponent } from './close/close.component';
import { ReassingComponent } from './reassing/reassing.component';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})

export class TicketsComponent implements OnDestroy, OnInit {
  dataTableConfig = {
    config: {
      type: 'tickets',
      base: this.tickets,
      api: 'getTickets',
      role: 3,
      order: [[0, 'desc']]
    },
    columns: [
      {
        display: 'Estatus',
        field: 'id_tickets',
        type: 'ticket_status'
      },
      {
        display: 'CLIENTE',
        field: 'client.name',
        type: 'ticket_client',
        orderable: false
      },
      {
        display: 'CONTACTO',
        field: 'client',
        type: 'ticket_contact',
        orderable: false
      },
      {
        display: 'FECHA DE CREACION',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'ACCIONES',
        field: '',
        type: 'actions',
        options: [
          {
            display: 'Historial',
            icon: 'fa fa-search',
            event: 'tickets.record',
            conditionality: 'true'
          },
          {
            display: 'Asignar ticket',
            icon: 'fa fa-hand-o-right',
            event: 'tickets.assign',
            conditionality: 'this.data.status === "opened"'
          },
          {
            display: 'Reasignar ticket',
            icon: 'fa fa-hand-o-right',
            event: 'tickets.reassing',
            conditionality: 'this.data.status === "assigned"'
          },
          {
            display: 'Desasignar ticket',
            icon: 'fa fa-hand-o-left',
            event: 'tickets.unassign',
            conditionality: 'this.data.status === "assigned"'
          },
          {
            display: 'Cerrar ticket',
            event: 'tickets.close',
            conditionality: 'this.data.status === "completed" || this.data.status === "confirmed" '
          }
        ]
      }
    ],
    filters: [
      {
        cssClass: 'col-md-12',
        type: 'status',
        options: [
          {
            val: '',
            name: 'TODOS'
          },
          {
            val: 'opened',
            name: 'ABIERTO'
          },
          {
            val: 'assigned',
            name: 'ASIGNADO'
          },
          {
            val: 'completed',
            name: 'COMPLETADO POR EL TECNICO'
          },
          {
            val: 'confirmed',
            name: 'FIRMADO POR EL CLIENTE'
          },
          {
            val: 'closed',
            name: 'CERRADO'
          }
        ]
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private readonly tickets: TicketsService,
    private readonly broadcast: BroadcastService,
    public appModal: ModalComponent,
    private readonly swal: SwalService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'tickets.record': this.recordItem(event.data); break;
        case 'tickets.assign': this.assignItem(event.data); break;
        case 'tickets.reassing': this.reassingItem(event.data); break;
        case 'tickets.unassign': this.unassignItem(event.data); break;
        case 'tickets.close': this.closeItem(event.data); break;
      }
    });

  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  recordItem(data): void {
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(RecordComponent, data, { title: `Historial del ticket ${data.id_tickets} del cliente ${data.client.name}` });
    this.appModal.openXl(props);
  }

  assignItem(data): void {
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(AssignComponent, data, { title: 'Asignación de ticket' });
    this.appModal.openXl(props);
  }

  reassingItem(data): void {
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(ReassingComponent, data, { title: 'Reasignación de ticket' });
    this.appModal.openXl(props);
  }

  unassignItem(data): void {
    const swalParams = {
      title: '¿Desea desasignar este ticket?',
      text: '¡Si desasigna este ticket el estatus pasara a abierto!'
    };

    this.swal.warning(swalParams).then(result => {
      if (result.value) {
        const params = {
          status: 'unassigned'
        };
        this.tickets.getStatus(params, data.id_tickets).subscribe((res: any) => {
          if (res.success) {
            this.swal.success().then(() => {
              this.activeModal.dismiss();
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({ title: 'Ocurrió un error al actualizar los datos' });
          }
        });
      }
    });
  }

  closeItem(data): void {
    // open modal, passing the context
    const props: SharedComponent = new SharedComponent(CloseComponent, data, { title: `Cerrar el ticket ${data.id_tickets} del cliente ${data.client.name}` });
    this.appModal.open(props);
  }

}
