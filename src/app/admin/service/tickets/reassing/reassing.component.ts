import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { SwalService } from '../../../../services/swal.service';
import { BroadcastService } from '../../../../services/broadcast.service';
import { TicketsService } from '../../../../services/tickets.service';

@Component({
  selector: 'app-reassing',
  templateUrl: './reassing.component.html',
  styleUrls: ['./reassing.component.scss']
})
export class ReassingComponent implements OnInit, OnDestroy {
  @Input() data: any;
  message: any;
  dataTableConfig = {
    config: {
      type: 'tickets-technitians',
      base: this.tickets,
      api: 'getTechnicians',
      order: [[0, 'desc']]
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Correo',
        field: 'email',
        type: 'text',
        orderable: false
      },
      {
        display: 'Teléfono',
        field: 'phone',
        type: 'text',
        orderable: false
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      }
    ],
    filters: [
      {
        cssClass: 'd-none',
        type: 'status',
        options: []
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private readonly broadcast: BroadcastService,
    public activeModal: NgbActiveModal,
    private readonly tickets: TicketsService,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'tickets-technitians-row-click': this.message = event.data; break;
      }
    }
    );
    // tslint:disable-next-line: no-console
    console.log(this.data);
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  assignTechnician(): void {
    const swalParams = {
      title: 'Instrucciones',
      text: 'Proporciona más detalles al técnico para un servico más rápido y eficiente.'
    };
    this.swal.input(swalParams).then(result => {
      if (result.value.trim() === '') {
        this.swal.error({ title: 'Debes especificar instrucciones' });
        
        return false;
      }

      if (result.value) {
        const params = {
          id_technicians: this.message.id,
          details: result.value,
          status: 'assigned'
        };
        this.tickets.getStatus(params, this.data.id_tickets).subscribe((data: any) => {
          if (data.success) {
            this.swal.success().then(() => {
              this.activeModal.dismiss();
              if ('isMap' in this.data) {
                this.broadcast.fire({
                  name: 'reload-map',
                  data: {}
                });
              } else {
                this.broadcast.reloadDataTable();
              }
            });
          } else {
            this.swal.error({ title: 'Ocurrió un error al actualizar los datos' });
          }
        });
      }
    });
  }

}
