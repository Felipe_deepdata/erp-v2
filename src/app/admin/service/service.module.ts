import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
import { ServiceRoutes } from './service.routing';
import { TicketsComponent } from './tickets/tickets.component';
import { ErrorCodesComponent } from './error-codes/error-codes.component';
import { AgmCoreModule } from '@agm/core';
import { TicketsMapComponent } from './tickets-map/tickets-map.component';

@NgModule({
  declarations: [
    TicketsComponent,
    TicketsMapComponent,
    ErrorCodesComponent
  ],
  exports: [
    TicketsComponent,
    TicketsMapComponent,
    ErrorCodesComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild(ServiceRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAXbhIbswUZIdCi-Yw3f1KhoTp3Eg_Y7q4'
      // libraries: ['places']
    })
  ]
})
export class ServiceModule { }
