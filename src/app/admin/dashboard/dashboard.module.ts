import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { KpiComponent } from '../dashboard/kpi/kpi.component';
import { DashboardTablesComponent } from '../dashboard/dashboard-tables/dashboard-tables.component';
import { IncomesChartComponent } from '../dashboard/incomes-chart/incomes-chart.component';
import { ChartsModule } from 'ng2-charts';
import { SubscriptionsChartComponent } from './subscriptions-chart/subscriptions-chart.component';
import { SharesTotalChartComponent } from './shares-total-chart/shares-total-chart.component';
import { SharesByTypeChartComponent } from './shares-by-type-chart/shares-by-type-chart.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ComponentsModule
  ],
  declarations: [
    DashboardComponent,
    DashboardTablesComponent,
    KpiComponent,
    IncomesChartComponent,
    SubscriptionsChartComponent,
    SharesTotalChartComponent,
    SharesByTypeChartComponent
  ],
  entryComponents: [],
  exports: [
    DashboardComponent
  ]
})
export class DashboardModule { }
