import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { ChartOptions, ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-shares-total-chart',
  templateUrl: './shares-total-chart.component.html',
  styleUrls: ['./shares-total-chart.component.scss']
})
export class SharesTotalChartComponent implements OnInit, OnChanges {
  @Input('data') total: any;
  total_shares = [];
  labels = [];
  options: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          offset: true,
          type: 'time',
          time: {
            unit: 'month'
          }
        }
      ],
      yAxes: [{
        offset: true
      }]
    }
  };

  totalShareChartData: Array<ChartDataSets> = [
    {
      data: this.total_shares,
      label: 'Total'
    }
  ];

  barChartType = 'bar';

  ngOnInit(): void {
    //
   }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.total.currentValue.total.length > 0) {
      this.total_shares = [];
      this.totalShareChartData[0].data = changes.total.currentValue.total.map((total: any) =>
        ({ x: total.month, y: total.quantity }));

      this.labels = changes.total.currentValue.total.map((total: any) =>
        total.month);
    }
  }

}
