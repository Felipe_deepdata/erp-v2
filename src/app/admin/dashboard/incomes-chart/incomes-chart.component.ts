import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-incomes-chart',
  templateUrl: './incomes-chart.component.html',
  styleUrls: ['./incomes-chart.component.scss']
})
export class IncomesChartComponent implements OnInit, OnChanges {
  @Input('data') incomes: any;
  actual_incomes = [];
  past_incomes = [];
  labels = [];
  months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

  options: ChartOptions = {
    scales: {
      xAxes: [{}],
      yAxes: [{}]
    }
  };
  incomesChartData: Array<ChartDataSets> = [
    {
      stack: 'a',
      data: this.actual_incomes,
      label: 'Ingresos actuales'
    },
    {
      stack: 'a',
      data: this.past_incomes,
      label: 'Ingresos pasados'
    }
  ];
  lineChartType = 'line';

  ngOnInit(): void {
    //
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.incomes.currentValue.actualIncomes.length > 0) {
      this.actual_incomes = [];
      this.incomesChartData[0].data = changes.incomes.currentValue.actualIncomes.map((income: any) =>
        ({ x: income.date, y: parseFloat(income.amount) }));

      this.labels = changes.incomes.currentValue.actualIncomes.map((income: any) => {
        const month = income.date.slice(5) - 1;
        const year = income.date.slice(2, 4);
        const month_label = `${this.months[month]} ${year}`;

        return month_label;
      });
    }

    if (changes.incomes.currentValue.pastIncomes.length > 0) {
      this.past_incomes = [];
      this.incomesChartData[1].data = changes.incomes.currentValue.pastIncomes.map((income: any) =>
        ({ x: income.date, y: parseFloat(income.amount) }));

      if (changes.incomes.currentValue.actualIncomes.length === 0) {
        this.labels = changes.incomes.currentValue.pastIncomes.map((income: any) => {
          const month = income.date.slice(5) - 1;
          const year = income.date.slice(2, 4);
          const month_label = `${this.months[month]} ${year}`;

          return month_label;
        });
      }
    }
  }

}
