import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { ChartOptions, ChartDataSets } from 'chart.js';
import { Color } from 'ng2-charts';

@Component({
  selector: 'app-shares-by-type-chart',
  templateUrl: './shares-by-type-chart.component.html',
  styleUrls: ['./shares-by-type-chart.component.scss']
})
export class SharesByTypeChartComponent implements OnInit, OnChanges {
  @Input('data') type: any;
  try_share = [];
  fb = [];
  wa = [];
  labels = [];
  options: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          offset: true,
          type: 'time',
          time: {
            unit: 'month'
          }
        }
      ],
      yAxes: [{
        offset: true,
        stacked: true
      }]
    }
  };

  sharesByTypeChartData: Array<ChartDataSets> = [
    {
      data: this.try_share,
      label: 'Intento compartir',
      stack: 'a'
    },
    {
      data: this.fb,
      label: 'Facebook',
      stack: 'a'
    },
    {
      data: this.wa,
      label: 'WhatsApp',
      stack: 'a'
    }
  ];
  barChartColors: Array<Color> = [
    { backgroundColor: 'orange' },
    { backgroundColor: 'blue' },
    { backgroundColor: '#2eb85c' }
  ];
  barChartType = 'bar';

  ngOnInit(): void {
    //
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.type.currentValue.type.length > 0) {
      this.sharesByTypeChartData[2].data = changes.type.currentValue.type
        .filter((try_share: any) =>
          try_share.type === 1)
        .map((try_share: any) =>
          ({ x: try_share.month, y: try_share.quantity }));

      this.sharesByTypeChartData[1].data = changes.type.currentValue.type
        .filter((fb: any) =>
          fb.type === 2)
        .map((fb: any) =>
          ({ x: fb.month, y: fb.quantity }));

      this.sharesByTypeChartData[0].data = changes.type.currentValue.type
        .filter((wa: any) =>
          wa.type === 3)
        .map((wa: any) =>
          ({ x: wa.month, y: wa.quantity }));
    }

  }

}
