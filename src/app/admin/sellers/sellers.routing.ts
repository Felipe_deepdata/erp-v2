import { Routes } from '@angular/router';
import { SalesGroupComponent } from './sales-group/sales-group.component';
import { ExtrasComponent } from './extras/extras.component';
import { CommissionsComponent } from './commissions/commissions.component';

export const SellersRoutes: Routes = [
    {
        path: 'comisiones',
        component: CommissionsComponent
    },
    {
        path: 'extras',
        component: ExtrasComponent
    },
    {
        path: 'grupos',
        component: SalesGroupComponent
    }
];
