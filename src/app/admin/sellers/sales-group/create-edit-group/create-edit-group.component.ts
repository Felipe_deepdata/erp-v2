import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FromService } from '../../../../providers/form.service';
import { SwalService } from '../../../../services/swal.service';
import { GroupsService } from '../../../../services/groups.service';

@Component({
  selector: 'app-create-edit-group',
  templateUrl: './create-edit-group.component.html',
  styleUrls: ['./create-edit-group.component.scss']
})
export class CreateEditGroupComponent implements OnInit {
  @Input() data: any;
  group: any;
  form: FormGroup = this.formBuilder.group({
    name: [''],
    deposit: ['', Validators.required],
    installation_fee: ['', Validators.required],
    trial_days: [''],
    trial_days_price: [''],
    monthly_fee: ['', Validators.required]
  });

  constructor(
    public activeModal: NgbActiveModal,
    private readonly groupService: GroupsService,
    private readonly formBuilder: FormBuilder,
    private readonly fromService: FromService,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    if (this.data.status === 'edit') {
      this.groupService.show(this.data.editData.id_groups).subscribe((resp: any) => {
        this.group = resp.response;
        Object.keys(this.group).forEach(key => {
          if (this.group.hasOwnProperty(key) && !!this.form.controls[key]) {
            this.form.controls[key].setValue(this.group[key]);
          }
        });
      });
    }
    this.fromService.setForm(this.form);
  }

  save(): void {
    if (this.data.status === 'edit') {
      this.updateGroup();
    }
    if (this.data.status === 'create') {
      this.saveGroup();
    }
  }

  saveGroup(): void {
    if (this.form.valid) {
      this.swal.warning({ title: '¿Esta seguro de querer guardar los datos del grupo?' }).then(result => {
        if (result.value) {
          this.groupService.create(this.form.value).subscribe((resp: any) => {
            if (resp.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
              });
            } else {
              this.swal.error({ title: 'Ocurió un error al guardar los datos' });
            }
          });
        }
      });
    }
  }

  updateGroup(): void {
    if (this.form.valid) {
      this.swal.warning({ title: '¿Esta seguro de querer actualizar los datos del grupo?' }).then(result => {
        if (result.value) {
          this.groupService.update(this.data.editData.id_groups, this.form.value).subscribe((resp: any) => {
            if (resp.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
              });
            } else {
              this.swal.error({ title: 'Ocurió un error al actualizar los datos' });
            }
          });
        }
      });
    }
  }

}
