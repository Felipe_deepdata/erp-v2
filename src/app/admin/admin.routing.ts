import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { DefaultLayoutComponent } from '../containers/default-layout/default-layout.component';
import { ClientsComponent } from './clients/clients.component';

export const AdminRoutes: Routes = [
    {
        path: 'admin',
        redirectTo: 'admin/inicio',
        pathMatch: 'full'
    },
    {
        path: 'admin',
        component: DefaultLayoutComponent,
        canActivate: [AuthGuardService],
        children: [
            { 
                path: 'inicio', 
                component: DashboardComponent
            },
            { 
                path: 'clientes',
                component: ClientsComponent
            },
            { 
                path: 'administracion',
                loadChildren: './administration/administration.module#AdministrationModule'
            },
            { 
                path: 'servicio',
                loadChildren: './service/service.module#ServiceModule'
            },
            {
                path: 'social',
                loadChildren: './social/social.module#SocialModule'
            },
            { 
                path: 'finanzas',
                loadChildren: './finances/finances.module#FinancesModule'
            },
            {
                path: 'vendedores',
                loadChildren: './sellers/sellers.module#SellersModule'
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ]
})

export class AdminModule {}
