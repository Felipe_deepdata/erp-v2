import { Routes } from '@angular/router';
import { AdministratorsComponent } from './administrators/administrators.component';

export const AdministrationRoutes: Routes = [
    {
        path: 'administradores',
        data: {role: 'Administrator', name: 'Administrador'},
        component: AdministratorsComponent
    }, 
    {
        path: 'tecnicos',
        data: {role: 'Technician', name: 'Técnico'},
        component: AdministratorsComponent
    }
];
