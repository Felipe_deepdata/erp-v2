import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

// Routing
import { SocialRoutes } from './social.routing';
// Components
import { ComponentsModule } from '../../components/components.module';
import { NotifyComponent } from './notify/notify.component';
import { NotifyUserComponent } from './notify/notify-user/notify-user.component';
import { NotifyGroupComponent } from './notify/notify-group/notify-group.component';

@NgModule({
  declarations: [
    NotifyComponent,
    NotifyUserComponent,
    NotifyGroupComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    NgbModule,
    RouterModule.forChild(SocialRoutes),
    ReactiveFormsModule
  ]
})
export class SocialModule { }
