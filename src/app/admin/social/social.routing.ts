import { Routes } from '@angular/router';
import { NotifyComponent } from './notify/notify.component';

export const SocialRoutes: Routes = [
    {
        path: 'notificaciones', 
        component: NotifyComponent
    } 
];
