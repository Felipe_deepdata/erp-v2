import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { BroadcastService } from '../../../../services/broadcast.service';
import { ModalComponent } from '../../../../components/modal/modal.component';
import { ChargesService } from '../../../../services/charges.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SwalService } from '../../../../services/swal.service';
import { DebtsService } from '../../../../services/debts.service';

@Component({
  selector: 'app-debts',
  templateUrl: './debts.component.html',
  styleUrls: ['./debts.component.scss']
})
export class DebtsComponent implements OnInit {
  @Input() data: any;
  dataTableDebts = {
    config: {
      base: this.debts,
      api: 'getClientDebts',
      params: {
        id_clients: ''
      }
    },
    columns: [
      {
        display: 'Creacion',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Monto',
        field: 'amount',
        type: 'number'
      },
      {
        display: 'Gastos de cobranza',
        field: 'collection_fees',
        type: 'number'
      },
      {
        display: 'Interés moratorio',
        field: 'moratory_fees',
        type: 'number'
      },
      {
        display: 'Total',
        field: 'id_clients',
        type: 'debt-total'
      },
      {
        display: '',
        field: '',
        type: 'inline-button',
        options: [
          {
            cssClass: 'btn btn-primary',
            name: 'Realizar cargo',
            event: 'debts.charge',
            conditionality: 'true'
          }
        ]
      }
    ]
  };
  dataTableCharges = {
    config: {
      base: this.charges,
      api: 'getClientCharges',
      params: {
        id_clients: ''
      }
    },
    columns: [
      {
        display: 'Fecha',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'ID del cargo',
        field: 'id_charges',
        type: 'text',
        orderable: false
      },
      {
        display: 'Descripción',
        field: 'description',
        type: 'text',
        orderable: false
      },
      {
        display: 'Monto',
        field: 'amount',
        type: 'text'
      },
      {
        display: 'Devolución',
        field: 'amount',
        type: 'text'
      },
      {
        display: 'Vendedor',
        field: 'vendor',
        type: 'text',
        orderable: false
      },
      {
        display: 'Tipo de cargo',
        field: 'charge_type',
        type: 'text',
        orderable: false
      },
      {
        display: 'Error',
        field: 'failure_message',
        type: 'text',
        orderable: false
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private readonly charges: ChargesService,
    private readonly debts: DebtsService,
    private readonly broadcast: BroadcastService,
    private readonly swal: SwalService,
    public appModal: ModalComponent,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.dataTableDebts.config.params.id_clients = this.data.id_clients;
    this.dataTableCharges.config.params.id_clients = this.data.id_clients;
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'debts.charge': this.debtsItem(event.data); break;
      }
    });
  }
  debtsItem(data): void {
    this.swal.warning({title: '¿Desea realizar este cargo?'}).then(result => {        
      if (result.value) {
        this.debts.chargeDebt(data.id_debts).subscribe((resp: any) => {
          if (resp.success) {
            this.swal.success().then(() => {
              this.activeModal.dismiss();
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({title: 'Ocurrió un error al actualizar los datos'});
          }
      });
      }
    });
  }
}
