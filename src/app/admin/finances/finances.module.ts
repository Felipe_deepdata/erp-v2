import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Routing
import { FinancesRoutes } from './finances.routing';
// Components
import { BadDebtsComponent } from './bad-debts/bad-debts.component';
import { ComponentsModule } from '../../components/components.module';
import { ClientsComponent } from './clients/clients.component';
import { InvoicesComponent } from './invoices/invoices.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild(FinancesRoutes)
  ],
  declarations: [
    BadDebtsComponent,
    ClientsComponent,
    InvoicesComponent
  ],
  exports: [
    BadDebtsComponent,
    ClientsComponent
  ]
})
export class FinancesModule { }
