import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { navItems } from '../../_nav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {
  navItems = navItems;
  sidebarMinimized = true;
  element: HTMLElement;
  private readonly changes: MutationObserver;

  constructor(
    private readonly router: Router,
    @Inject(DOCUMENT) _document?: any) {

    this.changes = new MutationObserver(mutations => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(this.element as Element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnInit(): void {
    //
  }
  
  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  logOut(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');
    this.router.navigate(['/login']).catch();
  }
}
