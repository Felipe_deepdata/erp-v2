// tslint:disable: file-name-casing
import { INavData } from '@coreui/angular';

export const navItems: Array<INavData> = [
  {
    name: 'Dashboard',
    url: '/admin/inicio',
    icon: 'fa fa-th-large'
  },
  {
    name: 'Clientes',
    url: '/admin/clientes',
    icon: 'fa fa-users'
  },
  {
    name: 'Servicio',
    url: '/admin/servicio',
    icon: 'fa fa-ticket',
    class: 'servicio',
    children: [
      { name: 'Tickets', url: '/admin/servicio/tickets' },
      { name: 'Mapa del seguimiento', url: '/admin/servicio/mapa_de_tickets' },
      { name: 'Códigos de error', url: '/admin/servicio/codigos_de_error' }
    ]
  },
  {
    name: 'Social',
    url: '/admin/social',
    icon: 'fa fa-twitter',
    children: [
      { name: 'Notificaciones', url: '/admin/social/notificaciones' }
    ]
  },
  {
    name: 'Finanzas',
    url: '/admin/finanzas',
    icon: 'fa fa-usd',
    children: [
      { name: 'Contratos', url: '/admin/finanzas/clientes' },
      { name: 'Responsabilidad Social', url: '/admin/finanzas/clientes' },
      { name: 'Facturas', url: '/admin/finanzas/facturas' },
      { name: 'Malas deudas', url: '/admin/finanzas/malas-deudas' }
    ]
  },
  {
    name: 'Vendedores',
    url: '/admin/vendedores',
    icon: 'fa fa-files-o',
    children: [
      // { name: 'Contratos', url: '/admin/vendedores/contratos' },
      { name: 'Extras', url: '/admin/vendedores/extras' },
      { name: 'Grupos', url: '/admin/vendedores/grupos' },
      { name: 'Comisiones', url: '/admin/vendedores/comisiones' }
    ]
  },
  {
    name: 'Administración',
    url: '/admin/administracion',
    icon: 'fa fa-user',
    children: [
      { name: 'Administradores', url: '/admin/administracion/administradores'},
      { name: 'Técnicos', url: '/admin/administracion/tecnicos' }
    ]
  },
  {
    name: 'Dev Only',
    url: '/admin/dev',
    icon: 'fa fa-bug',
    children: [
      // { name: 'Galeria', url: '/admin/social/galeria' },
      { name: 'Asignación', url: '/dev/asignacion' },
      { name: 'Encuestas', url: '/dev/encuestas' }
    ]
  }
];
